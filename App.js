/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React, {Component} from 'react';
import MainNavigation from './src/navigation/MainNavigation';
import {View} from 'react-native';

export default class App extends Component {
  render() {
    return <MainNavigation/>;
    // return <View/>
  }
}
