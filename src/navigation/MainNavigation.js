/**
 * @format
 */


import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignUp from '../components/SignUp';
import SignIn from '../components/SignIn'
import TabNavigator from './TabNavigator'

import * as RouteName from  '../utils/routerName'


const Stack = createStackNavigator();
export default function MainNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name={RouteName.SIGN_IN} component={SignIn} />
        <Stack.Screen name={RouteName.SIGN_UP} component={SignUp} />
        <Stack.Screen name={RouteName.TAB_NAVIGATOR} component={TabNavigator}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}