import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screen/Home';
import Setting from '../screen/Setting';
import Account from '../screen/Account';

import * as RouteName from '../utils/routerName';

const Tab = createBottomTabNavigator();

export default function TabNavigator() {
  return (
    <Tab.Navigator>
      <Tab.Screen name={RouteName.HOME} component={Home} />
      <Tab.Screen name={RouteName.ACCOUNT} component={Account} />
      <Tab.Screen name={RouteName.SETTING} component={Setting} />
    </Tab.Navigator>
  );
}
