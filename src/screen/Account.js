/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  View,
  Text,
} from 'react-native';
import * as styles from '../assest/styleSheets/home';

export default class Account extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text> This is Account Page</Text>
      </View>
    );
  }
}
