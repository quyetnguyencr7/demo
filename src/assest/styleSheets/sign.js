import {Dimensions} from 'react-native'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export const container = {
  flex: 1,
  backgroundColor: '#455a64',
  alignItems: 'center',
  justifyContent: 'center',
};
export const title = {
  color: 'white',
  fontSize: 20,
  marginBottom: 40,
};
export const input = {
  width: width * 0.7,
  backgroundColor: 'rgba(255,255,255,0.3)',
  borderRadius: 50,
  marginTop: 20,
  padding: 15,
  fontSize: 14,
  color: 'white',
};
export const logo = {
  width: 100,
  height: height/7,
  marginBottom: 10,
  marginTop: 40,
};
export const btnText = {
  fontSize: 18,
  fontWeight: 'bold',
  color: 'white',
};
export const button = {
  backgroundColor: '#1c313a',
  borderRadius: 50,
  width: width * 0.7,
  marginTop: 30,
  padding: 12,
  alignItems: 'center',
  justifyContent: 'center',
};

export const signUp = {
  flexGrow: 1,
  alignItems: 'flex-end',
  justifyContent: 'center',
  marginVertical: 16,
  flexDirection: 'row',
};
export const signUpText = {
  color: 'rgba(255,255,255,0.6)',
  fontSize: 16,
};

export const signUpBtn = {
  color: '#ffffff',
  marginLeft: 10,
  fontSize: 18,
  fontWeight: 'bold',
};
