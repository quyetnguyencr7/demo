/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import logo from '../assest/images/logo.png';
import * as styles from '../assest/styleSheets/sign';




export default class SignUp extends Component {
  constructor(props){
    super(props);
  }

  handelClickSignIn = () => {
    this.props.navigation.navigate('signIn');
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={logo} style={styles.logo} />
        <Text style={styles.title}> Wellcome to APP</Text>
        <TextInput
          style={styles.input}
          placeholder="Email"
          placeholderTextColor="white"
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="white"
        />
        <TouchableOpacity style={styles.button}>
          <Text style={styles.btnText}>SignUp</Text>
        </TouchableOpacity>
        <View style={styles.signUp}>
          <Text style={styles.signUpText}>Don't have an account yet?</Text>
          <TouchableOpacity onPress={() => this.handelClickSignIn()}>
            <Text style={styles.signUpBtn}>SignIn</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


