/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import logo from '../assest/images/logo.png';
import * as styles from '../assest/styleSheets/sign';
import { SIGN_UP, TAB_NAVIGATOR} from  '../utils/routerName'


export default class SignIn extends Component {
  constructor(props) {
    super(props);
  }
  handelClickChangeRoute = (routerName) => {
    this.props.navigation.navigate(routerName)
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={logo} style={styles.logo} />
        <Text style={styles.title}> Wellcome to APP</Text>
        <TextInput
          style={styles.input}
          placeholder="Email"
          placeholderTextColor="white"
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="white"
        />
        <TouchableOpacity style={styles.button}>
          <Text style={styles.btnText} onPress={() => this.handelClickChangeRoute(TAB_NAVIGATOR)}>LogIn</Text>
        </TouchableOpacity>
        <View style={styles.signUp}>
          <Text style={styles.signUpText}>Don't have an account yet?</Text>
          <TouchableOpacity onPress={() => this.handelClickChangeRoute(SIGN_UP)}>
            <Text style={styles.signUpBtn} >Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
